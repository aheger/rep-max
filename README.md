# rep-max

Max Repetition calculator for weightlifting.  

[Demo](https://aheger.gitlab.io/rep-max/)

## Project Goals

* provide a way to calculate the repetition max for weightlifting for 1-12 lifts
* use [Svelte](https://svelte.technology/)
* use [Redux](http://redux.js.org/)
* use Flexbox responsive design

## Usage

### Start the dev-server

```
npm start
```

### Create build
```
npm run build
```
Creates a build in ./target/build

### Create release build
```
npm run release
```
Creates an optimized release build in ./target/release
