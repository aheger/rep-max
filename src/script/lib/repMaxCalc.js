const FORMULAS = Object.freeze({
		BRZYCKI: (weight, reps) => weight * (36.0 / (37.0 - reps)),
		EPLEY: (weight, reps) => weight * (1.0 + reps / 30.0),
		LANDER: (weight, reps) => (100 * weight) / (101.3 - 2.67123 * reps)
	}),
	FORMULAS_INV = Object.freeze({
		BRZYCKI: (repMax, reps) => (repMax * (37.0 - reps)) / 36.0,
		EPLEY: (repMax, reps) => repMax / (1.0 + reps / 30.0),
		LANDER: (repMax, reps) => (repMax * (101.3 - 2.67123 * reps)) / 100
	});

function _getFormulaAverage(fns) {
	const formulas = Object.values(fns),
		args = Array.prototype.slice.call(arguments, 1);

	const ret = formulas.reduce(function(sum, fn) {
		return sum + fn.apply(null, args);
	}, 0) / formulas.length;

	return Math.round(ret * 10) / 10;
}

export const getRepMax = _getFormulaAverage.bind(null, FORMULAS);
export const getWeight = _getFormulaAverage.bind(null, FORMULAS_INV);

