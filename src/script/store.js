import { createStore } from "redux";

const initialState = {
	repMax: 0
};

const reducers = (state = initialState, action) => {
	switch (action.type) {
		case "REPMAX_SET":
			return Object.assign({}, state, { repMax: action.payLoad });
		default:
			return state;
	}
};

export default createStore(reducers);
