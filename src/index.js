import App from "./component/app.svelte";

import "./style/_vars.css";
import "./style/index.css";
import "./style/app.css";

new App({
	target: document.querySelector("#app")
});
